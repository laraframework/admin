<?php

return [
	'column' => [
		'cgroup' => 'groep',
		'key' => 'key',
		'lang' => 'taal',
		'tag' => 'categorie',
		'value' => 'waarde',
	],
	'entity' => [
		'edit_translation' => 'vertaling bewerken',
		'entity_plural' => 'vertalingen',
		'entity_single' => 'vertaling',
		'entity_title' => 'vertalingen',
	],
	'form' => [
		'search' => 'zoek key',
	],
	'general' => [
		'all_translations' => 'alle vertalingen',
	],
	'message' => [
		'exported_to_file' => 'vertalingen geëxporteerd naar bestand',
		'overwrite_database' => 'alle vertalingen in de database worden overschreven!',
		'overwrite_files' => 'alle taalbestanden worden overschreven!',
	],
];
