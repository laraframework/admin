<?php

return [
	'boxtitle' => [
		'analytics' => 'analytics',
		'browser_stats' => 'browser statistieken',
		'content_items' => 'content items',
		'page_stats' => 'pagina statistieken',
		'refresh_cache' => 'cache verversen',
		'ref_stats' => 'referral statistieken',
		'site_stats' => 'site statistieken',
		'users' => 'gebruikers',
		'user_stats' => 'gebruiker statistieken',
	],
	'button' => [
		'cache_clear' => 'cache verwijderen',
	],
	'cache' => [
		'type' => 'type',
	],
	'entity' => [
		'entity_title' => 'dashboard',
	],
	'message' => [
		'last_sync' => 'laatst bijgewerkt',
		'nodata' => 'geen data beschikbaar',
	],
];
