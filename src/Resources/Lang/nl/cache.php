<?php

return [
	'button' => [
		'cache_clear' => 'cache verwijderen',
	],
	'column' => [
		'type' => 'type',
	],
	'message' => [
		'cache_flush_warning' => 'alle cache van de applicatie wordt verwijderd',
		'please_wait' => 'even geduld',
	],
];
