<?php

return [
	'column' => [
		'content' => 'content',
		'locked_by_admin' => 'vergrendelen',
		'page' => 'pagina',
		'parent' => 'parent',
		'route' => 'route',
		'route_has_auth' => 'afschermen',
		'slug' => 'slug',
		'status' => 'status',
		'title' => 'titel',
		'type' => 'type',
		'view' => 'template',
	],
	'entity' => [
		'entity_title' => 'menu',
	],
	'message' => [
		'create_new_page' => 'maak nieuwe pagina',
	],
	'value' => [
		'type_entity' => 'module',
		'type_form' => 'formulier',
		'type_page' => 'pagina',
		'type_parent' => 'folder',
		'type_root' => 'root',
		'type_url' => 'url',
	],
];
