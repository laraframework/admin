<?php

return [
	'column' => [
		'redirectfrom' => 'redirect van',
		'redirectto' => 'redirect naar',
		'redirecttype' => 'redirect type',
		'source' => 'bron',
	],
	'entity' => [
		'entity_plural' => 'redirects',
		'entity_single' => 'redirect',
		'entity_title' => 'redirects',
	],
];
