<?php

return [
	'column' => [
		'body' => 'tekst',
		'lead' => 'inleiding',
		'locked_by_admin' => 'vergrendelen',
		'parent' => 'parent',
		'title' => 'titel',
	],
	'entity' => [
		'entity_plural' => 'categorieën',
		'entity_single' => 'categorie',
		'entity_title' => 'categorieën',
	],
];
