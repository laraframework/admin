<?php

return [
	'boxtitle' => [
		'roles' => 'rollen',
	],
	'column' => [
		'email' => 'email',
		'firstname' => 'voornaam',
		'isloggedin' => 'ingelogd',
		'language' => 'taal',
		'lastlogin' => 'laatst ingelogd',
		'lastname' => 'achternaam',
		'level' => 'niveau',
		'middlename' => 'tussenvoegsel',
		'name' => 'volledige naam',
		'new_password' => 'nieuw wachtwoord',
		'password' => 'wachtwoord',
		'role' => 'rol',
		'roles' => 'rollen',
		'username' => 'gebruikersnaam',
	],
	'entity' => [
		'entity_plural' => 'gebruikers',
		'entity_single' => 'gebruiker',
		'entity_title' => 'gebruikers',
	],
	'message' => [
		'user_already_exists' => 'fout: de gebruikersnaam bestaat al',
	],
	'profile' => [
		'link_text' => 'mijn profiel',
		'profile_title' => 'mijn profiel',
	],
];
