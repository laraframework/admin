<?php

return [
	'column' => [
		'position' => 'positie',
		'slug' => 'slug',
		'title' => 'titel',
	],
	'entity' => [
		'entity_plural' => 'entiteit groepen',
		'entity_single' => 'entiteit groep',
		'entity_title' => 'entiteit groepen',
	],
];
