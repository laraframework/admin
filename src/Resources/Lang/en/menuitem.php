<?php

return [
	'column' => [
		'content' => 'content',
		'locked_by_admin' => 'lock',
		'page' => 'page',
		'parent' => 'parent',
		'route' => 'route',
		'route_has_auth' => 'protected',
		'slug' => 'slug',
		'status' => 'status',
		'title' => 'title',
		'type' => 'type',
		'view' => 'view',
	],
	'entity' => [
		'entity_title' => 'menu',
	],
	'message' => [
		'create_new_page' => 'create new page',
	],
	'value' => [
		'type_entity' => 'module',
		'type_form' => 'form',
		'type_page' => 'page',
		'type_parent' => 'folder',
		'type_root' => 'root',
		'type_url' => 'url',
	],
];
