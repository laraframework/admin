<?php

return [
	'boxtitle' => [
		'analytics' => 'analytics',
		'browser_stats' => 'browser statistics',
		'content_items' => 'content items',
		'page_stats' => 'page statistics',
		'refresh_cache' => 'refresh cache',
		'ref_stats' => 'referral statistics',
		'site_stats' => 'site statistics',
		'users' => 'users',
		'user_stats' => 'user statistics',
	],
	'button' => [
		'cache_clear' => 'clear cache',
	],
	'cache' => [
		'type' => 'type',
	],
	'entity' => [
		'entity_title' => 'dashboard',
	],
	'message' => [
		'last_sync' => 'last update',
		'nodata' => 'no data available',
	],
];
