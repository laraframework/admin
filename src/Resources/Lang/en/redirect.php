<?php

return [
	'column' => [
		'redirectfrom' => 'redirect from',
		'redirectto' => 'redirect to',
		'redirecttype' => 'redirect type',
		'source' => 'source',
	],
	'entity' => [
		'entity_plural' => 'redirects',
		'entity_single' => 'redirect',
		'entity_title' => 'redirects',
	],
];
