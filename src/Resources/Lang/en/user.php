<?php

return [
	'boxtitle' => [
		'roles' => 'roles',
	],
	'column' => [
		'email' => 'email',
		'firstname' => 'first name',
		'isloggedin' => 'logged in',
		'language' => 'language',
		'lastlogin' => 'last login',
		'lastname' => 'last name',
		'level' => 'level',
		'middlename' => 'middle name',
		'name' => 'display name',
		'new_password' => 'new password',
		'password' => 'password',
		'role' => 'role',
		'roles' => 'roles',
		'username' => 'username',
	],
	'entity' => [
		'entity_plural' => 'users',
		'entity_single' => 'user',
		'entity_title' => 'users',
	],
	'message' => [
		'user_already_exists' => 'error: the username already exists',
	],
	'profile' => [
		'link_text' => 'my profile',
		'profile_title' => 'my profile',
	],
];
