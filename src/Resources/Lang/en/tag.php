<?php

return [
	'column' => [
		'body' => 'body',
		'lead' => 'lead',
		'locked_by_admin' => 'lock',
		'parent' => 'parent',
		'title' => 'title',
	],
	'entity' => [
		'entity_plural' => 'tags',
		'entity_single' => 'tag',
		'entity_title' => 'tags',
	],
];
