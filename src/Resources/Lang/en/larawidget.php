<?php

return [
	'boxtitle' => [
		'modulepages' => 'module page',
		'pages' => 'page',
	],
	'column' => [
		'body' => 'text',
		'customtags' => 'customtags',
		'filtertaxonomy' => 'filtertaxonomy',
		'hook' => 'hook',
		'iconalign' => 'icon align',
		'iconclass' => 'icon class',
		'imgreq' => 'imgreq',
		'isglobal' => 'isglobal',
		'linktext' => 'link text',
		'linkurl' => 'link url',
		'maxitems' => 'maxitems',
		'relentkey' => 'relentkey',
		'sortorder' => 'sortorder',
		'template' => 'template',
		'title' => 'title',
		'type' => 'type',
		'usecache' => 'usecache',
	],
	'entity' => [
		'entity_plural' => 'widgets',
		'entity_single' => 'widget',
		'entity_title' => 'widgets',
	],
	'messages' => [
		'back_to_page' => 'back to page',
	],
];
