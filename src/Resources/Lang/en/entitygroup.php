<?php

return [
	'column' => [
		'position' => 'position',
		'slug' => 'slug',
		'title' => 'title',
	],
	'entity' => [
		'entity_plural' => 'entity groups',
		'entity_single' => 'entity group',
		'entity_title' => 'entity groups',
	],
];
