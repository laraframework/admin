<?php

return [
	'button' => [
		'cache_clear' => 'clear cache',
	],
	'column' => [
		'type' => 'type',
	],
	'message' => [
		'cache_flush_warning' => 'all application cache will be flushed',
		'please_wait' => 'please wait',
	],
];
